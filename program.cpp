#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('o');
	mylist.pushToHead('s');
	mylist.pushToHead('h');
	mylist.pushToTail('c');
	mylist.pushToTail('n');
	mylist.pushToTail('e');
	mylist.pushToTail('K');
	cout << "Current : ";
	mylist.print();
	cout << endl;
	mylist.reverse();
	cout << "Reverse : ";
	mylist.print();//
	cout << endl;
	mylist.popTail();
	cout << "After PopTail : ";
	mylist.print();
	cout << endl;
	char input;
	cout << "Input a character for Search : ";
	cin >> input;
	if (mylist.search(input) == true) 
	{
		cout << "Character found!\n";
	}
	else {
		cout << "Character didn't found!\n";
	}
	system("pause");
	

}